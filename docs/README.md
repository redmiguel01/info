# Coronavirus Makers

Hub de información general sobre la iniciativa de Coronavirus Makers.

## Onboarding

_Me gustaría ayudar, ¿qué puedo hacer?_

¡Gracias por mostrar interés! Existen muchas iniciativas en marcha en las que puedes colaborar:

* Quiero [hacer I+D](iniciativas/iniciativas_imasd.md)
* Quiero [fabricar/imprimir](iniciativas/iniciativas_fabricar.md)
* Quiero [crear usando TIC](iniciativas/iniciativas_tic.md)
* Quiero [apoyar a los makers](iniciativas/iniciativas_apoyomakers.md)
* Quiero [conversar](iniciativas/iniciativas_conversar.md)
* Quiero [saber quiénes sois](iniciativas/iniciativas_informacion.md)
* Quiero [hacer esto en mi pais](iniciativas/iniciativas_internacional.md)

Puedes ayudar la información que tenemos en este sitio, y [aquí te explicamos cómo](CONTRIBUTING.md)

Para contribuir o inspirarte con alguna iniciativa, echa un vistazo a estos ejemplos:

|Tipo        |Iniciativa                                        |URL|
|------------|--------------------------------------------------|---|
|Información |Web principal makers                              |<https://www.coronavirusmakers.org/>|
|Información |Covid Valencia                                    |<http://covidvalencia.org/>|
|Fabricación |Portal 3D covid                                   |<https://portal3dcovid19.es/>|
|Comunicación|Foro makers                                       |<https://foro.coronavirusmakers.org/>|
|Comunicación|Grupos de Telegram                                |<https://kumu.io/lahoramaker/coronavirusmakers-nacional#grupos-y-relaciones-con-otras-iniciativas/cv19fabcanalinfo>|
|Logística   |App de gestión de fabricación Corredor del Henares|<https://creator.zohopublic.eu/zoho_pavel36/cv19-corredor-henares/page-perma/Portal/UGOWe3RXY7hwDNYvmebg2x6v04k8FQ8pFbJ1hSUSzEsZ65PHD5VdtOJZgys72DHtCDTdR1BPSrExF5RyM8U4qEF812aFz420y2Hv>|
|Logística   |Coronavirus 3d print                              |<https://coronavirus3dprint.com/>|
|Contacto    |Mensageo                                          |<https://mensageo.com/>|
|HUB         |Esta Documentación                                |<https://gitlab.com/coronavirusmakers/info>|

El proceso es muy sencillo:

* Si todavía no tienes usuario en GitLab, regístrate: es gratis.
* Abre una tarea nueva en este proyecto con tu usuario:
    * En el título de la tarea pon la iniciativa a la que quieres ayudar
    * En la descripción añade en qué puedes ayudar.
    * Selecciona la etiqueta "Quiero ayudar".
* Espera y ten paciencia.

Nos podremos en contacto contigo cuando sea posible.

Si ya estás participando en alguna iniciativa y necesitas ayuda, abre una tarea con etiqueta "busco ayuda".

## Código de conduca

Revisa nuestro [código de conducta](CODE_OF_CONDUCT.md).
