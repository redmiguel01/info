# Telegram: Grupo Principal de Makers

Se trata del grupo principal de makers

Enlace al grupo: https://t.me/CV_FAB_BAL

Mensaje anclado a fecha 25/03/2020

    BIENVENIDO, LEE ESTE MENSAJE Y NO METAS RUIDO AL ENTRAR 😘😘

    📭 Telegram: @coronavirus_makers
    Grupo principal (mantenerlo en silencio, evitar el flood)

    🤼‍♂️ Telegram: Web con listado actualizado de los +40 grupos temáticos existentes
    Visítala, apúntate en los que creas conveniente, lee primero, y no metas ruido.

    🖨 Si tienes impresora 3D debes hacer dos cosas: usa este formulario  para dar de alta tu impresora y apúntate en el grupo local de tu comunidad autónoma o país. También apúntate si tienes CNC o inyección de moldes.

    PAÍSES
    Alemania
    Grupo 
    Andorra
    Canal 

    Argentina:
    Nacional, canal, Protecciones faciales,  respiradores
    Caba, Ciudad Autónoma de Buenos Aires, Comodoro Rivadavia, Córdoba, Corrientes, La Plata, Mar del Plata, Mendoza, Provincia de Buenos Aires (Zona norte, Zona oeste), Salta, Santa Cruz, Santa Fe, Tierra del Fuego, Tucumán 

    Chile:
    Grupo 
    Colombia:
    Grupo
    Costa Rica
    Grupo 
    Cuba
    Grupo 
    Ecuador
    Grupo 

    España:
    Andalucia (Almería, Cádiz, Granada, Huelva, Jaén, Málaga, Sevilla) Aragon (Huesca, Teruel, Zaragoza), Asturias (Avilés, Cuencas, Oviedo, Gijón), Baleares, Castilla y León, Cataluña, Canarias, Cantabria, Castilla La Mancha (Torrijos), Comunidad Valenciana (Castellón, Valencia, Alicante), Extremadura, Galicia, Madrid, Murcia, Navarra, Rioja, País Vasco, Melilla, Ceuta

    Italia
    Sardegna 
    México
    Grupo
    Panamá
    Grupo
    Paraguay
    Grupo  
    Perú
    Grupo
    República Dominicana
    Grupo 
    Reino Unido
    Grupo 
    Suiza
    Grupo 
    Venezuela:
    Canal 

    Entra también en los grupos de las áreas que te puedan interesar

    - SOLUCIONES SANITARIAS: Soluciones sanitarias 
    - RECURSOS GENERALES: Anuncios, Bibliografía, Alternativas, Dudas 
    - GRUPOS DE PROTECCIÓN: Mascarillas y filtro, Viseras-Pantallas, Gafas, Cascos, Agarres - manecillas
    - GRUPO DE RESPIRADORES: respiradores
    - DISEÑO: Diseño
    - INSTRUMENTACIÓN: Capnógrafo-Medidor de CO2 
    - ELECTRÓNICA: electrónica
    - SOFTWARE: software

    🐚 Telegram: @CV19Makers_Anuncios
    Canal de anuncios (solo noticias importantes y resúmenes diarios, unas 5 al día) 

    📕 Foro de información organizada: https://foro.coronavirusmakers.org/
    Si quieres colaborar en el foro organizando información entra e inscríbete directamente